# 三国文游

 
### 介绍

这是一个以三国题材的文字图片游戏插件


### 访问量
<br><img src="https://count.getloli.com/get/@:xyb12345678qwe?theme=rule33" /><br>
### 安装教程

输入命令:<br>  
在云崽根目录输入以下命令
````
git clone https://gitee.com/xyb12345678qwe/sanguo-plugin.git ./plugins/sanguo-plugin/

pnpm i

````

### 使用说明

请安装插件后对机器人输入`#三国帮助`，查看可用功能

更新三国后请使用`#三国同步`

如何转移存档，请保存以下存档
![输入图片说明](./resources/img/readme/image.png)
![输入图片说明](./resources/img/readme/image2.png)

### 交流群

QQ群：910914597


### 作者相关

作者: 名字
个人博客: [戳我前往](https://boke.mzswebs.top/) 

作者：hbj白夜
个人博客：[戳我前往](https://hbj2457.cn/) 

### 额外说明

本插件禁止用于盈利

也禁止魔改版开库（私下自己玩可以）

觉得功能少的可以反馈给作者，更新会比较慢~

有想来自愿开发的可以来找作者