import { existplayer,Read_player,Write_player,Add_player,__PATH,get_player_img,Write_json,Read_json
    ,alluser,Go,getPlayerAction,isNotNull,Read_yaml,Write_yaml,jianmaster } from "../game/sanguo.js";
import data from '../../model/sanguoData.js'
import fs from 'fs';
export class GM extends plugin {
    constructor() {
      super({
        /** 功能名称 */
        name: '三国|游戏管理',
        /** 功能描述 */
        dsc: '管理模块',
        event: 'message',
        /** 优先级，数字越小等级越高 */
        priority: 600,
        rule: [
          {
            reg: /^(#|\/)结束状态.*$/,
            fnc: 'jieshu',
          },
          {
            reg: /^(#|\/)升级国家.*$/,
            fnc: 'rise',
          },
          {
            reg: /^(#|\/)查看国家可升级项.*$/,
            fnc: 'check',
          },
          {
            reg: /^(#|\/)添加管理员.*$/,
            fnc: 'jia',
          },
        ]
      });
    }
    async jia(e){
      if (!e.isMaster) return;
      let isAt = e.message.some(item => item.type === 'at');
      let yaml = await Read_yaml(1);
      
      let qq;
      if (isAt) {
        let atItems = e.message.filter(item => item.type === 'at');
        let B_qq = atItems[0].qq;
        const hasElement = yaml.master.includes(B_qq);
        if(hasElement) return e.reply(`已经是管理员`)
        yaml.master.push(B_qq); // 将 master 属性设置为包含 B_qq 的数组
        qq = B_qq;
        await Write_yaml(1, yaml);
      } else {
        let name = e.msg.replace(/\/添加管理员|#添加管理员/g, '');
        const hasElement = yaml.master.includes(name);
        if(hasElement) return e.reply(`已经是管理员`)
        yaml.master.push(name); // 将 master 属性设置为包含 name 的数组
        qq = name;
        await Write_yaml(1, yaml);
      }
      await Write_yaml(1, yaml);
      return e.reply(`加入管理员 ${qq}`);      
    }
    async jieshu(e){
      if(!await jianmaster(e)) return;
      //对方
      let isat = e.message.some(item => item.type === 'at');
      if (!isat) return;
      let atItem = e.message.filter(item => item.type === 'at'); //获取at信息
      let B_qq = atItem[0].qq; //对方qq
      //检查存档
      let ifexistplay = await existplayer(1,B_qq);
      if (!ifexistplay) return e.reply('对方无存档');
      let action = await redis.get(`sanguo:player:${B_qq}:action`);
      action = JSON.parse(action); //转为json格式数据
      if (!action) return e.reply(`对方无状态`);
      await redis.del(`sanguo:player:${B_qq}:action`);
      e.reply(`已结束对方状态`)
      return;
    }
    async rise(e){
        if(!await jianmaster(e)) return;
        let qian ;
        if(e.msg.includes("/")){
            qian = e.msg.replace("/升级国家", '');
        }else if(e.msg.includes("#")){
            qian = e.msg.replace("#升级国家", '');
        }
        qian = qian.trim();
        let code = qian.split('*');
        let guo = code[0].trim(); // 国家名称
        let thing_name = code[1].trim(); // 升级名称
        let num = code[2].trim(); //升级数
        let guojia
        if (guo ==="曹魏" ) {
            guojia = await Read_json(1)
        }else if (guo==='蜀汉') {
            guojia = await Read_json(3)
        }else if(guo ==="东吴"){
            guojia = await Read_json(2)
        }
        if (guo!=="曹魏"&&guo!=="蜀汉"&&guo!=="东吴") {
            e.reply(`没有${guo}这个国家`)
            return;
        }else if (thing_name !== "人口"&&thing_name !== "经济"&&thing_name !== "科技"&&thing_name !== "科技exp"&&thing_name!=="兵力") {
            e.reply(`${thing_name}不可升级`)
            return;
        }
        guojia[thing_name] += num
        if (guo ==="曹魏" ) {
            await Write_json(1,guojia)
        }else if (guo==='蜀汉') {
            await Write_json(3,guojia)
        }else if(guo ==="东吴"){
            await Write_json(2,guojia)
        }
        e.reply(`${guo}${thing_name}升级成功`)
        return;
    }
    async check(e){
        e.reply(`人口\n经济\n科技\n科技exp\n兵力`)
        return;
    }
}