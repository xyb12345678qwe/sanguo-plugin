import{synchronization}from'./game/sanguo.js'
export class bottongbu extends plugin {
    constructor() {
      super({
        name: '三国|三国同步',
        dsc: '三国同步',
        event: 'message',
        priority: 600,
        rule: [
          {
            reg: /^(#|\/)三国同步.*/,
            fnc: 'tongbu',
          },
        ],
      });
    }
    async tongbu(e){
      await synchronization(e)
      return;
    }
}