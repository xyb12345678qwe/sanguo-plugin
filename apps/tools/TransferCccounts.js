import { existplayer,Read_player,Write_player,Add_player,__PATH,get_player_img,Write_json,Read_json
    ,alluser,Go,getPlayerAction,isNotNull,Read_yaml,Write_yaml,jianmaster } from "../game/sanguo.js";
import data from '../../model/sanguoData.js'
import fs from 'fs';
import { userInfo } from "os";
export class zuanzhang extends plugin {
    constructor() {
      super({
        /** 功能名称 */
        name: '三国|转账模块',
        /** 功能描述 */
        dsc: '转账',
        event: 'message',
        /** 优先级，数字越小等级越高 */
        priority: 600,
        rule: [
          {
            reg: /^(#|\/)转帐.*$/,
            fnc: `z`,
          },
        ]
      });
    }
    async z (e){
        const usr_qq = e.user_id;
        let player = await Read_player(1, usr_qq);
        const name = e.msg.replace(/\/转帐|#转帐/g, '');
        const atItems = e.message.filter(item => item.type === 'at');
        if (!atItems) return;
        const B_qq = atItems[0].qq;
        if(B_qq === usr_qq) return e.reply(`不能给自己转账`)
        let B_player = await Read_player(1, B_qq);
        const transferAmount = parseInt(name);
        if (player.金钱 < transferAmount) return e.reply(`金钱不足`);
        B_player.金钱 += transferAmount;
        player.金钱 -= transferAmount;
        await Write_player(1,usr_qq,player) 
        await Write_player(1,usr_qq,B_player)
        return e.reply(`成功转账${transferAmount}`); 
    }
}