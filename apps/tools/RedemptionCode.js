import { existplayer,Read_player,Write_player,Add_player,__PATH,get_player_img,Write_json,Read_json
    ,alluser,Go,getPlayerAction,isNotNull,getGuojiaIndex,getGuojia,hasPassedOneDay } from "../game/sanguo.js";
import data from '../../model/sanguoData.js'
import fs from 'fs';
export class dui extends plugin {
    constructor() {
      super({
        /** 功能名称 */
        name: '三国|兑换码模块',
        /** 功能描述 */
        dsc: '兑换码',
        event: 'message',
        /** 优先级，数字越小等级越高 */
        priority: 600,
        rule: [
          {
            reg: /^(#|\/)兑换码.*$/,
            fnc: 'tiqu',
          },
          {
            reg: /^(#|\/)查看开局兑换码.*$/,
            fnc: 'dui',
          },
          {
            reg: /^(#|\/)stop.*$/,
            fnc: 's',
          }
        ]
      });
    }
    async s(e){
       const usr_qq = e.user_id;
      await redis.del(`xiuxian:duihuan:${usr_qq}`);; //兑换码
      return e.reply(`stop成功`);
    }
    async dui(e){
      return e.reply(`/兑换码三国开局礼包`)
    }
    async tiqu(e){
        const usr_qq = e.user_id;
        const ifexistplay2 = await existplayer(1,usr_qq);
        if (!ifexistplay2) {
         return e.reply('无存档');
        }
        let player = await Read_player(1,usr_qq)

        let qian;
        if (e.msg.includes("/")) {
        qian = e.msg.replace("/兑换码", '');
        } else if (e.msg.includes("#")) {
        qian = e.msg.replace("#兑换码", '');
        }
        qian = qian.trim();
        console.log(qian);
        // console.log(await data.duihuanma_list);
        // console.log(await data.duihuanma_list.find(item => item.name === qian));
        let ma = await data.duihuanma_list.find(item => item.name === qian);
        console.log(ma);
        if (!isNotNull(ma)) {
        e.reply("没有这个兑换码");
        return;
        }

        let action = await redis.get(`xiuxian:duihuan:${usr_qq}`);; //兑换码
        action = await JSON.parse(action);
         if (action == null||action === undefined) {
          action = [];
        }
       
        if (action) {
          console.log(action.length);
          for (let i = 0; i < action.length; i++) {
            console.log(action[i]);
            action[i]=qian
            e.reply(`此对话码已经兑换过了`)
            return;
         }
        }
        action.push(qian);
        await redis.set(`xiuxian:duihuan:${usr_qq}`,JSON.stringify(action));
        const itemList = [
            'wujiang_list',
            ];
    
            let results = [];
            if (ma.金钱 != 0) {
                player.金钱 += ma.金钱
                results.push(`金钱 * ${ma.金钱}`);
            }
            if (ma.物品 != '') {
                e.reply(`没有开发完`)
                return;
             for (let i = 0; i < ma.物品.length; i++) {
                const currentItem = ma.物品[i];
                const itemName = currentItem.name;
                const itemQuantity = currentItem.数量;
                for (const listName of itemList) {
                    const item = await data[listName].find(item => item.name === itemName);
                    if (item) {
                    await Add_najie_thing(usr_qq, item.name, item.class, itemQuantity);
                    results.push(`${itemName} * ${itemQuantity}`);
                    break;
                }
            }
            }
    }
            if (results.length > 0) {
            e.reply(`成功兑换:\n${results.join('\n')}`);
            }
            await Write_player(1,usr_qq,player)
            return;
    }
}