import { plugin, puppeteer } from '../../api/api.js';
import Help from '../../model/help.js';
import md5 from 'md5';

let helpData = {
  md5: '',
  img: '',
};
export class botHelp extends plugin {
    constructor() {
      super({
        /** 功能名称 */
        name: '三国|三国帮助',
        /** 功能描述 */
        dsc: '三国帮助',
        event: 'message',
        /** 优先级，数字越小等级越高 */
        priority: 400,
        rule: [
          {
            reg: '^(#|/)三国帮助$',
            fnc: 'sanguohelp',
          }
        ],
      });
    }
    async sanguohelp(e){
        if (!e.isGroup) {
            return;
          }
          let data = await Help.get(e);
          if (!data) return;
          let img = await this.cache(data);
          await e.reply(img);
    }
    async cache(data) {
        let tmp = md5(JSON.stringify(data));
        if (helpData.md5 == tmp) return helpData.img;
        helpData.img = await puppeteer.screenshot('help', data);
        helpData.md5 = tmp;
        return helpData.img;
      }
}