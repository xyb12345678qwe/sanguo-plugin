import { existplayer,Read_player,Write_player,Add_player,__PATH,get_player_img,Write_json,Read_json
    ,alluser,Go,getPlayerAction,isNotNull,getGuojiaIndex,getGuojia,hasPassedOneDay } from "../game/sanguo.js";
import data from '../../model/sanguoData.js'
import fs from 'fs';
export class fushui extends plugin {
    constructor() {
      super({
        /** 功能名称 */
        name: '三国|赋税模块',
        /** 功能描述 */
        dsc: '赋税模块',
        event: 'message',
        /** 优先级，数字越小等级越高 */
        priority: 600,
        rule: [
          {
            reg: /^(#|\/)收取赋税$/,
            fnc: 'shouqu',
          },
          {
            reg: /^(#|\/)提取赋税.*$/,
            fnc: 'tiqu',
          }
        ]
      });
    }
    async shouqu(e){
      const usr_qq = e.user_id;
      let exist = await existplayer(1, usr_qq);
      if (!exist) {
        e.reply(`没有存档`);
        return false;
      }
      let player = await Read_player(1, usr_qq);
      let guojia = await getGuojia(player.国家);
      if (!isNotNull(guojia.赋税)) {
        e.reply(`请让管理员来输入#三国同步`);
        return;
      }
      console.log(await hasPassedOneDay(guojia.time,3));
      
       if (!await hasPassedOneDay(guojia.time,3)) {
          e.reply(`？急啥，冷却没到`)
          return;
        }
      if (guojia.人口 <= 0) {
        e.reply(`没有人口收啥税`);
        return;
      }
      
      // 计算赋税的理论值
      const taxRate = 6000; // 每人口的税金
      const technologyFactor = guojia.科技 === 0 ? 1 : guojia.科技;
      guojia.赋税 = parseInt(guojia.赋税)
      guojia.赋税 = parseInt(guojia.人口 * taxRate * technologyFactor*0.1);
      const currentTimeStamp = new Date().getTime();
      guojia.time = currentTimeStamp
      await Write_json(await getGuojiaIndex(player.国家), guojia);
      e.reply(`收取成功收取了${guojia.赋税}`)
      return;
    }
    async tiqu(e){
      const usr_qq = e.user_id;
      let exist = await existplayer(1, usr_qq);
      if (!exist) {
        e.reply(`没有存档`);
        return false;
      }
      let weizhi ;
        if(e.msg.includes("/")){
            weizhi = e.msg.replace("/提取赋税", '');
        }else if(e.msg.includes("#")){
          weizhi = e.msg.replace("#提取赋税", '');
        }
      let player = await Read_player(1, usr_qq);
      let guojia = await getGuojia(player.国家);
      if (weizhi === '') {
        e.reply(`请在命令后加数字`)
        return;
      }
      if (!isNotNull(guojia.赋税)) {
        e.reply(`请让管理员来输入#三国同步`);
        return;
      }
      if (guojia.赋税 < weizhi) {
        e.reply(`赋税不足`);
        return;
      }
      player.金钱=parseInt(player.金钱)
      player.金钱 += parseInt(weizhi);
      guojia.赋税=parseInt(guojia.赋税)
      guojia.赋税 -=parseInt(weizhi);
      if (guojia.赋税 === null || guojia.赋税 === undefined) {
        guojia.赋税 = 0
      }
      await Write_json(await getGuojiaIndex(player.国家), guojia);
      await Write_player(1,usr_qq,player)
      e.reply(`提取赋税成功`)
      return;
    }
  }