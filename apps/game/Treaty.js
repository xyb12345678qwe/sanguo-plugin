import { existplayer,Read_player,Write_player,Add_player,__PATH,get_player_img,Write_json,Read_json
    ,alluser,Go,Go2,Go3,getPlayerAction,isNotNull,getGuojiaIndex,getGuojia,hasPassedOneDay,getfuben } from "../game/sanguo.js";
import data from '../../model/sanguoData.js'
import fs from 'fs';
import { start } from "repl";
import { time } from "console";
export class tiaoyue extends plugin {
    constructor() {
      super({
        /** 功能名称 */
        name: '三国|条约模块',
        /** 功能描述 */
        dsc: '条约模块',
        event: 'message',
        /** 优先级，数字越小等级越高 */
        priority: 600,
        rule: [
          {
            reg: /^(#|\/)创建条约.*$/,
            fnc: 'jian',
          },
          {
            reg: /^(#|\/)修改条约.*$/,
            fnc: 'gai',
          },
          {
            reg: /^(#|\/)战.*$/,
            fnc: 'z',
          },
          {
            reg: /^(#|\/)查看国家状态.*$/,
            fnc: 'c',
          },
        ]
      });
    }
    async c(e){
        const usr_qq = e.user_id;
        if (!await existplayer(1, usr_qq))return e.reply(`没有存档`);
        let player = await Read_player(1, usr_qq);
        let guojia = await getGuojia(player.国家);
        if (!await Go3(e,guojia)) return;
        return e.reply(`空闲中`);
    }
    async z(e){
        const usr_qq = e.user_id;
        if (!await existplayer(1, usr_qq))return e.reply(`没有存档`);
        let player = await Read_player(1, usr_qq);
        let guojia = await getGuojia(player.国家);
        if (!await Go3(e,guojia)) return;
        let name = e.msg.includes('/') ? e.msg.replace('/战', '') : e.msg.includes('#') ? e.msg.replace('#战', '') : ''.trim();
        let code = name.split('*');
        let [tiaoyue,国家] = code.map(item => item.trim()); 
        let msg = [];
        if(国家 !== '东吴'&&国家 !== '蜀汉'&&国家 !=='曹魏')return e.reply(`没有这个国家`)
        let guojia2 = await getGuojia(国家);
        let x = guojia.条约.find(team =>toString(team.name) === toString(tiaoyue));
        if(!x) msg.push(`没有此条约`);
        if (msg.length > 0) return e.reply(msg)
        if(x.签订国 !=="无") return e.reply(`此条约已使用`)
        if(x.划分国土 > guojia2.国土面积) return e.reply(`请更换条约，对方没有这么多土地`)
        const currentTimeStamp = new Date().getTime();
        let action={
            action:"国家战争",
            start_time:currentTimeStamp,
            guo1:guojia.name,
            guo2:国家,
            tiaoyue:x,
            time:5
        }
        await redis.set(`sanguo:${guojia.name}:action`,
        JSON.stringify(action));
        return e.reply(`开始打战`)
    }
    async jian(e){
        const usr_qq = e.user_id;
        if (!await existplayer(1, usr_qq))return e.reply(`没有存档`);
        let player = await Read_player(1, usr_qq);
        let guojia = await getGuojia(player.国家);
        let name = e.msg.includes('/') ? e.msg.replace('/创建条约', '') : e.msg.includes('#') ? e.msg.replace('#创建条约', '') : ''.trim();
        if(guojia.条约.find(team =>team.name === name))return e.reply(`已有此条约`)
        let tiao = 
        {
            name:name,
            划分国土:0,
            赔偿金额:0,
            签订国:"无",
        }
        guojia.条约.push(tiao)
        await Write_json(await getGuojiaIndex(player.国家), guojia);
        return e.reply(`条约创建成功`)
    }
    async gai(e){
        const usr_qq = e.user_id;
        if (!await existplayer(1, usr_qq))return e.reply(`没有存档`); 
        let player = await Read_player(1, usr_qq);
        let guojia = await getGuojia(player.国家);
        let n = e.msg.includes('/') ? e.msg.replace('/修改条约', '') : e.msg.includes('#') ? e.msg.replace('#修改条约', '') : ''.trim();
        let code = n.split('*');
        let [shu, ming, num] = code.map(item => item.trim()); // 解构赋值并去除空格
        let x = guojia.条约.find(team => toString(team.name) === toString(ming));
        if (!x) return e.reply(`没有此条约`);
        const allowedProperties = ["名字", "划分国土", "赔偿金额"];
        if (!allowedProperties.includes(shu)) return e.reply(`没有这个属性`);
        if (shu === "名字") shu = "name";
        if (!isNaN(num)) num = parseInt(num) 
        x[shu] = num;
        await Write_json(await getGuojiaIndex(player.国家), guojia);
        return e.reply(`修改完毕`);
    }
}