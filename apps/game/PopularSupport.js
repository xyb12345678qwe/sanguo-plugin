import { existplayer,Read_player,Write_player,Add_player,__PATH,get_player_img,Write_json,Read_json
    ,alluser,Go,getPlayerAction,isNotNull,getGuojiaIndex,getGuojia,hasPassedOneDay } from "../game/sanguo.js";
import data from '../../model/sanguoData.js'
import fs from 'fs';
export class duihuanma extends plugin {
    constructor() {
      super({
        /** 功能名称 */
        name: '三国|建房模块',
        /** 功能描述 */
        dsc: '建房模块',
        event: 'message',
        /** 优先级，数字越小等级越高 */
        priority: 600,
        rule: [
          {
            reg: /^(#|\/)建房.*$/,
            fnc: 'jian',
          },
        ]
      });
    }
    async jian(e){
        const usr_qq = e.user_id;
        const ifexistplay2 = await existplayer(1,usr_qq);
        if (!ifexistplay2) return e.reply('无存档');
        let player = await Read_player(1,usr_qq)
        let qian;
        if (e.msg.includes("/")) {
        qian = e.msg.replace("/建房", '');
        } else if (e.msg.includes("#")) {
        qian = e.msg.replace("#建房", '');
        }
        qian = qian.trim();
        qian = parseInt(qian)
        let x = 1000000
        qian *= x
        let q = player.金钱
        if (q < qian) return e.reply(`钱不够，还需要${qian - q}`)
        let guojia = await getGuojia(player.国家);
        q -=qian
        guojia.民心 += qian/x
        e.reply(`民心增加${qian/x}`)
        await Write_player(1,usr_qq,player)
        await Write_json(await getGuojiaIndex(player.国家), guojia);
        return;
    }
}