import { existplayer,Read_player,Write_player,Add_player,__PATH,get_player_img,Write_json,Read_json
    ,alluser,Go,getPlayerAction,isNotNull,getGuojiaIndex,getGuojia,hasPassedOneDay } from "../game/sanguo.js";
import data from '../../model/sanguoData.js'
import fs from 'fs';
export class zhengbing extends plugin {
    constructor() {
      super({
        /** 功能名称 */
        name: '三国|征兵模块',
        /** 功能描述 */
        dsc: '征兵模块',
        event: 'message',
        /** 优先级，数字越小等级越高 */
        priority: 600,
        rule: [
          {
            reg: /^(#|\/)强制征兵$/,
            fnc: 'zheng',
          }
        ]
      });
    }
    async zheng(e){
        const usr_qq = e.user_id;
        let exist = await existplayer(1, usr_qq)
        if (!exist) {
            e.reply(`没有存档`)
            return false;
        }
        let player =await Read_player(1,usr_qq)
        let guojia = await getGuojia(player.国家)
        if (guojia.人口 === 0) {
            e.reply(`没有人口争个屁个兵`)
            return;
        }
        if (!isNotNull(guojia.民心)) {
            e.reply(`请让管理员来输入#三国同步`);
            return;
        }
        if (!await hasPassedOneDay(guojia.time2,7)) {
            e.reply(`？急啥，冷却没到`)
            return;
        }
        let num = guojia.人口 *= 0.6
        const populationIncrease = Math.floor(Math.random() * num);
        guojia.兵 += populationIncrease
        guojia.人口 -= populationIncrease
        guojia.民心 -= 10
        e.reply(`此次征兵${populationIncrease}人，丧失民心10点(民心共100点，丧失完会发动起义)`)
        const currentTimeStamp = new Date().getTime();
        guojia.time2 = currentTimeStamp
        await Write_json(await getGuojiaIndex(player.国家), guojia);
        return;
    }
}