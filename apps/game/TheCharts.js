import { existplayer,Read_player,Write_player,Add_player,__PATH,get_player_img,Write_json,Read_json
    ,alluser,Go,getPlayerAction,isNotNull,getpaihangbang } from "../game/sanguo.js";
import data from '../../model/sanguoData.js'
import fs from 'fs';
export class paihangbang extends plugin {
    constructor() {
      super({
        /** 功能名称 */
        name: '三国|排行模块',
        /** 功能描述 */
        dsc: '排行模块',
        event: 'message',
        /** 优先级，数字越小等级越高 */
        priority: 600,
        rule: [
          {
            reg: /^(#|\/)武将战力排行榜$/,
            fnc: 'bang',
          },
        ]
      });
    }
    async bang(e){
        let weizhi = data.wujiang_list
        await getpaihangbang(e, weizhi)
        return;
    }
}