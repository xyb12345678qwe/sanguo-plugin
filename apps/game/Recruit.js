import { existplayer,Read_player,Write_player,Add_player,__PATH,get_player_img,Write_json,Read_json
  ,alluser,Go,getPlayerAction,isNotNull,getGuojiaIndex,getGuojia,Add_json_thing } from "../game/sanguo.js";
import data from '../../model/sanguoData.js'
import fs from 'fs';
export class UserStart extends plugin {
  constructor() {
    super({
      /** 功能名称 */
      name: '三国|招募模块',
      /** 功能描述 */
      dsc: '招募模块',
      event: 'message',
      /** 优先级，数字越小等级越高 */
      priority: 600,
      rule: [
        {
          reg: /^(#|\/)招募武将$/,
          fnc: 'zhao',
        },
      ]
    });
  }
  // 修改后的 zhao 函数  
  async zhao(e){  
    const usr_qq = e.user_id;  
    let exist = await existplayer(1, usr_qq)  
    if (!exist) {  
        e.reply(`没有存档`)  
        return false;  
    }  
    let player = await Read_player(1, usr_qq)  
    let guojia = await getGuojia(player.国家)  
    if (player.拥有金钱 < 100000) {  
        e.reply(`钱不够无法招募武将`)  
        return;  
    }  
    const choujiangList = data.wujiangzhaomu_list;  
    const randomNum = Math.random();  
    if (randomNum < 0.6) {  
        e.reply(`没有招募到什么东西`)  
        return;  
    }  
    // 获取"低级"、"中级"和"高级"部分的数据  
    const lowLevelData = choujiangList.find(item => item.hasOwnProperty("低级"));  
    const midLevelData = choujiangList.find(item => item.hasOwnProperty("中级"));  
    const highLevelData = choujiangList.find(item => item.hasOwnProperty("高级"));

    if (lowLevelData && midLevelData && highLevelData) {  
        const lowLevelItems = Object.entries(lowLevelData["低级"]);  
        const midLevelItems = Object.entries(midLevelData["中级"]);  
        const highLevelItems = Object.entries(highLevelData["高级"]);

        // 设定概率权重  
        const lowLevelWeight = 0.7;   // 低级概率权重为 70%  
        const midLevelWeight = 0.4;   // 中级概率权重为 50%  
        const highLevelWeight = 0.2;  // 高级概率权重为 30%  
        let results = [];

        // 根据概率随机选择物品级别  
        const randomNum = Math.random();  
        let selectedLevel;  
        if (randomNum < lowLevelWeight / totalWeight) {  
            selectedLevel = "低级";  
        } else if (randomNum < (lowLevelWeight + midLevelWeight) / totalWeight) {  
            selectedLevel = "中级";  
        } else {  
            selectedLevel = "高级";  
        }

        // 根据选定的物品级别随机选择物品  
        let selectedItems;  
        switch (selectedLevel) {  
            case "低级":  
                selectedItems = lowLevelItems;  
                break;  
            case "中级":  
                selectedItems = midLevelItems;  
                break;  
            case "高级":  
                selectedItems = highLevelItems;  
                break;  
        }

        // 随机选择物品
        if (selectedItems.length > 0) {
          const randomIndex = Math.floor(Math.random() * selectedItems.length);
          const [itemId, itemName] = selectedItems[randomIndex];
          console.log(`招募到的武将是是：${itemName}`);
      
          const itemList = [
            'wujiang_list',
          ];
      
          for (const listName of itemList) {
            const item = await data[listName].find(item => item.name === itemName);
            if (item) {
              Add_json_thing(usr_qq,item.name,"武将",1)
            }
          }
          player.金钱 -=100000
          await Write_player(1,usr_qq,player)
          await Write_json(await getGuojiaIndex(player.国家), guojia);
          e.reply(`随机选取的物品是：${itemName}`);
        } else {
          e.reply(`${selectedLevel}部分没有物品`);
        }
      }
  }
}