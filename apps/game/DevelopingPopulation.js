import { existplayer,Read_player,Write_player,Add_player,__PATH,get_player_img,Write_json,Read_json
    ,alluser,Go,getPlayerAction,isNotNull,getGuojiaIndex,getGuojia } from "../game/sanguo.js";
import data from '../../model/sanguoData.js'
import fs from 'fs';
export class fazhan extends plugin {
    constructor() {
      super({
        /** 功能名称 */
        name: '三国|发展模块',
        /** 功能描述 */
        dsc: '人口发展',
        event: 'message',
        /** 优先级，数字越小等级越高 */
        priority: 600,
        rule: [
          {
            reg: /^(#|\/)低级发展人口$/,
            fnc: 'fazhan1',
          },
          {
            reg: /^(#|\/)中级发展人口$/,
            fnc: 'zfazhan1',
          },
           {
            reg: /^(#|\/)高级发展人口$/,
            fnc: 'gfazhan1',
          },
          {
            reg: /^(#|\/)低级招兵$/,
            fnc: 'dfazhan2',
          },
          {
            reg: /^(#|\/)中级招兵$/,
            fnc: 'zfazhan2',
          },
          {
            reg: /^(#|\/)高级招兵$/,
            fnc: 'gfazhan2',
          },
          {
            reg: /^(#|\/)研发科技$/,
            fnc: 'yanfakeji',
          },
           {
            reg: /^(#|\/)突破科技$/,
            fnc: 'yanfakeji2',
          },
        ]
      });
    }
    async yanfakeji2(e){
        const usr_qq = e.user_id;
        console.log(usr_qq);
        let exist = await existplayer(1, usr_qq)
        if (!exist) {
            e.reply(`没有存档`)
            return false;
        }
        let player =await Read_player(1,usr_qq)
        let guojia = await getGuojia(player.国家)
        if (guojia.科技exp<1000) {
            e.reply(`科技经验不够`)
            return;
        }
        e.reply(`突破成功`)
        guojia.科技+=1
        await Write_json(await getGuojiaIndex(player.国家), guojia);
        return;
    }
    async yanfakeji(e){
        const usr_qq = e.user_id;
        console.log(usr_qq);
        let exist = await existplayer(1, usr_qq)
        if (!exist) {
            e.reply(`没有存档`)
            return false;
        }
        let player =await Read_player(1,usr_qq)
        let guojia = await getGuojia(player.国家)
        if (guojia.人口<10000) {
            e.reply(`人口这么点先去发展人口吧`)
            return;
        }
        if (player.金钱<1000000) {
            e.reply(`钱不够无法研究科技`)
            return;
        }
        let num = guojia.科技/100
        const successProbability = 0.3+num; // 设置成功的概率阈值
        const random = Math.random()+ num/10; 
        const random2 = Math.floor(Math.random() * 300);
        if (random < successProbability) {
          // 研究科技成功的逻辑
          guojia.科技exp += random2
          e.reply(`研究科技成功！科技经验加`);
        } else {
          // 研究科技失败的逻辑
          e.reply(`研究科技失败！`);
        }
        player.金钱 -= 1000000;
        await Write_player(1,usr_qq,player);
        await Write_json(await getGuojiaIndex(player.国家), guojia);
        return;
    }
    async gfazhan2(e){
        const usr_qq = e.user_id;
        console.log(usr_qq);
        let exist = await existplayer(1, usr_qq)
        if (!exist) {
            e.reply(`没有存档`)
            return false;
        }
        let player =await Read_player(1,usr_qq)
        let guojia = await getGuojia(player.国家)
        
        console.log(guojia);
        if (guojia.人口<10000000) {
            e.reply(`人口这么点先去发展人口吧`)
            return;
        }
        if (player.金钱<10000000) {
            e.reply(`钱不够无法招兵`)
            return;
        }
        console.log(123);
        const populationIncrease = Math.floor(Math.random() * guojia.人口);
        const randomDecrease = Math.floor(Math.random() * guojia.人口) - 8770000;
        const finalPopulationIncrease = Math.abs(populationIncrease - randomDecrease);

        const recruitmentValue = Math.floor(finalPopulationIncrease / 1000);
        guojia.兵 += recruitmentValue;

        let msg = [`成功招兵，招募值为：${recruitmentValue}`];
        console.log(123);
        const random = Math.random();
        let num2 = recruitmentValue;
        console.log(123);
        if (random < 0.6) {
        num2 *= 12;
        } else if (random < 0.7) {
        num2 *= 13;
        } else if (random < 0.6) {
        num2 *= 14;
        }
        console.log(123);
        guojia.兵力 += num2;
        player.金钱 -= 10000000;
        await Write_player(1,usr_qq,player);
        await Write_json(await getGuojiaIndex(player.国家), guojia);
        msg.push(`,兵力为${num2}`)
        e.reply(msg);
        return;
    }
    async zfazhan(e){
        const usr_qq = e.user_id;
        console.log(usr_qq);
        let exist = await existplayer(1, usr_qq)
        if (!exist) {
            e.reply(`没有存档`)
            return false;
        }
        let player =await Read_player(1,usr_qq)
        let guojia = await getGuojia(player.国家)
        
        console.log(guojia);
        if (guojia.人口<1000000) {
            e.reply(`人口这么点先去发展人口吧`)
            return;
        }
        if (player.金钱<1000000) {
            e.reply(`钱不够无法招兵`)
            return;
        }
        console.log(123);
        const populationIncrease = Math.floor(Math.random() * guojia.人口);
        const randomDecrease = Math.floor(Math.random() * guojia.人口) - 770000;
        const finalPopulationIncrease = Math.abs(populationIncrease - randomDecrease);

        const recruitmentValue = Math.floor(finalPopulationIncrease / 1000);
        guojia.兵 += recruitmentValue;

        let msg = [`成功招兵，招募值为：${recruitmentValue}`];
        console.log(123);
        const random = Math.random();
        let num2 = recruitmentValue;
        console.log(123);
        if (random < 0.7) {
        num2 *= 11;
        } else if (random < 0.6) {
        num2 *= 12;
        } else if (random < 0.4) {
        num2 *= 13;
        }
        console.log(123);
        guojia.兵力 += num2;
        player.金钱 -= 1000000;
        await Write_player(1,usr_qq,player);
        await Write_json(await getGuojiaIndex(player.国家), guojia);
        msg.push(`,兵力为${num2}`)
        e.reply(msg);
        return;
    }
    async dfazhan2(e){
        const usr_qq = e.user_id;
        console.log(usr_qq);
        let exist = await existplayer(1, usr_qq)
        if (!exist) {
            e.reply(`没有存档`)
            return false;
        }
        let player =await Read_player(1,usr_qq)
        let guojia = await getGuojia(player.国家)
        
        console.log(guojia);
        if (guojia.人口<100000) {
            e.reply(`人口这么点先去发展人口吧`)
            return;
        }
        if (player.金钱<100000) {
            e.reply(`钱不够无法招兵`)
            return;
        }
        console.log(123);
        const populationIncrease = Math.floor(Math.random() * guojia.人口);
        const randomDecrease = Math.floor(Math.random() * guojia.人口) - 70000;
        const finalPopulationIncrease = Math.abs(populationIncrease - randomDecrease);

        const recruitmentValue = Math.floor(finalPopulationIncrease / 1000);
        guojia.兵 += recruitmentValue;

        let msg = [`成功招兵，招募值为：${recruitmentValue}`];
        console.log(123);
        const random = Math.random();
        let num2 = recruitmentValue;
        console.log(123);
        if (random < 0.9) {
        num2 *= 10;
        } else if (random < 0.5) {
        num2 *= 11;
        } else if (random < 0.4) {
        num2 *= 12;
        }
        console.log(123);
        guojia.兵力 += num2;
        player.金钱 -= 100000;
        await Write_player(1,usr_qq,player);
        await Write_json(await getGuojiaIndex(player.国家), guojia);
        msg.push(`,兵力为${num2}`)
        e.reply(msg);
        return;
    }
    async fazhan1(e){
        const usr_qq = e.user_id;
        if (!(await existplayer(1, usr_qq))) {
            return false;
        }
        let player =await Read_player(1,usr_qq)
        let guojia = await getGuojia(player.国家)
        if (player.金钱 < 100000) {
            e.reply(`钱不够先去打工吧`)
            return;
        }
        // let num1 = guojia.人口
        const populationIncrease = Math.floor(Math.random() * 50000);
        guojia.人口 += populationIncrease;
        // const result = Math.floor(guojia.人口 - num1 / 10000);
        player.金钱 -= 100000;
        await Write_player(1,usr_qq,player);
        await Write_json(await getGuojiaIndex(player.国家), guojia);
        e.reply(`发展成功，国家人口增加了 ${populationIncrease}。`);
        return;
    }
    async zfazhan1(e){
        const usr_qq = e.user_id;
        if (!(await existplayer(1, usr_qq))) {
            return false;
        }
       
        let player =await Read_player(1,usr_qq)
        let guojia = await getGuojia(player.国家)
        if (player.金钱 < 1000000) {
            e.reply(`钱不够先去打工吧`)
            return;
        }
        // let num1 = guojia.人口
        const populationIncrease = Math.floor(Math.random() * 200000);
        guojia.人口 += populationIncrease;
        // const result = Math.floor(guojia.人口 - num1 / 10000);
        player.金钱 -= 1000000;
        await Write_player(1,usr_qq,player);
        await Write_json(await getGuojiaIndex(player.国家), guojia);
        e.reply(`发展成功，国家人口增加了 ${populationIncrease}。`);
        return;
    }
    async gfazhan1(e){
        const usr_qq = e.user_id;
        if (!(await existplayer(1, usr_qq))) {
            return false;
        }
        
        let player =await Read_player(1,usr_qq)
        let guojia = await getGuojia(player.国家)
        if (player.金钱 < 10000000) {
            e.reply(`钱不够先去打工吧`)
            return;
        }
        // let num1 = guojia.人口
        const populationIncrease = Math.floor(Math.random() * 2000000);
        guojia.人口 += populationIncrease;
        // const result = Math.floor(guojia.人口 - num1 / 10000);
        await Write_json(await getGuojiaIndex(player.国家), guojia);
        player.金钱 -= 1000000;
        await Write_player(1,usr_qq,player);
        e.reply(`发展成功，国家人口增加了 ${populationIncrease}。`);
        return;
    } 
}