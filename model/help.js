import base from './base.js';
import xiuxianCfg from './Config.js';

export default class Help extends base {
  constructor(e) {
    super(e);
    this.model = 'help';
  }

  static async get(e) {
    let html = new Help(e);
    return await html.getData();
  }

  async getData() {
    let helpData = xiuxianCfg.getdefSet('help', 'help');
    return {
      ...this.screenData,
      saveId: 'help',
      helpData,
    };
  }  
}