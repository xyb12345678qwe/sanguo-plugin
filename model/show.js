import base from './base.js';
import puppeteer from '../../../lib/puppeteer/puppeteer.js';
export default class Game extends base {
  constructor(e) {
    super(e);
    this.model = 'show';
  }

  async get_playerData(myData) {
    this.model = 'player';
    return {
      ...this.screenData,
      saveId: 'player',
      ...myData,
    };
  }
  async get_paihangbangData(myData) {
    this.model = 'paihangbang';
    return {
      ...this.screenData,
      saveId: 'paihangbang',
      ...myData,
    };
  }
  async get_fubenData(myData) {
    this.model = 'fuben';
    return {
      ...this.screenData,
      saveId: 'fuben',
      ...myData,
    };
  }
  async get_jiesuanData(myData) {
    this.model = 'jiesuan';
    return {
      ...this.screenData,
      saveId: 'jiesuan',
      ...myData,
    };
  }
  async get_guojia(myData) {
    this.model = 'guojia';
    return {
      ...this.screenData,
      saveId: 'guojia',
      ...myData,
    };
  }
  async get_guojiaduiwu(myData) {
    this.model = 'guojiaduiwu';
    return {
      ...this.screenData,
      saveId: 'guojiaduiwu',
      ...myData,
    };
  }
  async get_guojiatiaoyue(myData) {
    this.model = 'guojiatiaoyue';
    return {
      ...this.screenData,
      saveId: 'guojiatiaoyue',
      ...myData,
    };
  }
  async get_shi(myData) {
    this.model = 'shi';
    return {
      ...this.screenData,
      saveId: 'shi',
      ...myData,
    };
  }
  async get_shici(myData) {
    this.model = 'shici';
    return {
      ...this.screenData,
      saveId: 'shici',
      ...myData,
    };
  }
  async get_shilun(myData) {
    this.model = 'shilun';
    return {
      ...this.screenData,
      saveId: 'shilun',
      ...myData,
    };
  }
}