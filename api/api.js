import plugin from '../../../lib/plugins/plugin.js';
import common from '../../../lib/common/common.js';
import puppeteer from '../../../lib/puppeteer/puppeteer.js';
import data from '../model/sanguoData.js'
import config from '../model/Config.js'
export { plugin, common, puppeteer ,data,config};

/**
 * 提取机器人依赖
 */