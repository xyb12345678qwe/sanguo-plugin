import fs from 'node:fs';
import path from 'path';
import { AppName } from '../app.config.js';
import {exist_json,Write_sanguo} from'../apps/game/sanguo.js'
export const appsOut = async ({ AppsName }) => {
  const firstName = `plugins/${AppName}`;
  const filepath = `./${firstName}/${AppsName}`;
  const name = [];
  const sum = [];
  const travel = (dir, callback) => {
    fs.readdirSync(dir).forEach(file => {
      if (file.search('.js') != -1) name.push(file.replace('.js', ''));
      let pathname = path.join(dir, file);
      if (fs.statSync(pathname).isDirectory()) {
        travel(pathname, callback);
      } else {
        callback(pathname);
      }
    });
  };
  travel(filepath, path => {
    if (path.search('.js') != -1) sum.push(path);
  })
  let apps = {};
  for (let item of sum) {
    let address = `..${item.replace(/\\/g, '/').replace(`${firstName}`, '')}`;
    let allExport = await import(address);
    let keys = Object.keys(allExport);
    keys.forEach(key => {
      if (allExport[key].prototype) {
        if (apps.hasOwnProperty(key))
          logger.info(
            `Template detection:已经存在class ${key}同名导出\n    ${address}`
          );
        apps[key] = allExport[key];
      } else {
        logger.info(
          `Template detection:存在非class属性${key}导出\n    ${address}`
        );
      }
    });
  }
  if (!await exist_json(1) &&!await exist_json(2)&&!await exist_json(3)) {
    await Write_sanguo("曹魏")
    await Write_sanguo("东吴")
    await Write_sanguo("蜀汉")
    logger.info(
      `三国文件创建成功`
    );
  } else if(!await exist_json(1)){
    await Write_sanguo("曹魏")
    logger.info(
      `创建文件曹魏.json`
    );
  }else if (!await exist_json(2)) {
    await Write_sanguo("东吴")
    logger.info(
      `创建文件东吴.json`
    );
  }else if(!await exist_json(3)){
    await Write_sanguo("蜀汉")
    logger.info(
      `创建文件蜀汉.json`
    );
  }
  return apps;
};